
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;


class PolarCoords extends JFrame {

	private static final long serialVersionUID = -8203440582994469232L;

	int w = 700;
	int h = 700;
	int cw = w/2;
	int ch = h/2;
	List<Prime> primes = null;
	int tPrime = 0;
	Image img = null;
	double paintCount = 22.910;
	
	double stepvalue = 0;
	double stepamount = 0.00001;
	
	
	
	public PolarCoords() {
		paintCount = 22;
		//super("My Name");
		generatePrimes();
		setSize(w, h);
		this.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {}
			public void keyPressed(KeyEvent e) {
				//System.out.println(e.getKeyCode());
				System.out.println("Count: " + paintCount);
				if(e.getKeyCode() == 37) {
					stepvalue -= stepamount;
				}
				if(e.getKeyCode() == 38) {
					paintCount -= stepamount;
				}
				if(e.getKeyCode() == 40) {
					paintCount += stepamount;
				}				
				if(e.getKeyCode() == 39) {
					stepvalue += stepamount;
				}
				if(e.getKeyCode() == 32) {
					stepvalue = 0;
				}
			}
			public void keyReleased(KeyEvent e) {}
			
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		img = createImage(w, h);
		while(true) {
			//tPrime = 0;
			//try {
			//	Thread.sleep(100);
			//} catch (InterruptedException e) {
			//	// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
			repaint();
		}
	}

	public void paint(Graphics g) {
		//System.out.println("RePainting");

		if(img != null) {
			Graphics g2 = img.getGraphics();
			g2.setColor(Color.black);
			g2.fillRect(0, 0, w, h);
			
			//tPrime = paintCount;
			
			int cwt = cw;
			int cht = ch;
			//drawPoint(g2, cwt, cht);
			
			double r = 0;
			double t = 0;
			tPrime = 0;
			for(int i = 1; i < 16000; i++) {
				r = (i / paintCount) + 1;
				t = ((double)((double)(i % paintCount) / paintCount) * 360);
				cwt = (int)(r * Math.cos(t));
				cht = (int)(r * Math.sin(t));
				//System.out.println("X: " + (cwt+cw) + " Y: " + (cht+ch));
				drawPoint(g2, cwt+cw, cht+ch);
			}
			//System.out.println("Count: " + paintCount);
		}
		g.drawImage(img, 0, 0, null);
		//System.out.println("Finished Painting");
		paintCount += stepvalue;
	}
	
	private void drawPoint(Graphics g, int x, int y) {
		Prime p = primes.get(tPrime);
		if(p.isPrime) {
			g.setColor(Color.white);
			g.drawRect(x, y, 1, 1);
		} else {
			g.setColor(Color.blue);
			//g.drawRect(x, y, 1, 1);
		}
		tPrime++;
	}
	
	private void generatePrimes() {
		System.out.println("Starting to Gen primes");
		primes = new ArrayList<Prime>();
		boolean isPrime = false;
		for(int i = 1; i < w*h*2; i++) {
			isPrime = true;
			for(int j = 2; j <= (int)Math.sqrt(i); j++) {
				if((i % j) == 0 && j != i) {
					isPrime = false;
					break;
				}
			}
			primes.add(new Prime(i, isPrime));

		}
		System.out.println("Finished Generating Primes");
	}

	static public void main(String[] args) {
		new PolarCoords();
	}

}
