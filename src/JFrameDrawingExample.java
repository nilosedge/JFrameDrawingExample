
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;


class JFrameDrawingExample extends JFrame {

	private static final long serialVersionUID = -8203440582994469232L;

	int w = 300;
	int h = 300;
	int cw = w/2;
	int ch = h/2;
	List<Prime> primes = null;
	int tPrime = 0;
	Image img = null;
	int paintCount = 0;
	
	
	public JFrameDrawingExample() {
		//super("My Name");
		generatePrimes();
		setSize(w, h+20);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		img = createImage(w, h+20);
		while(true) {
			repaint();
		}
	}

	public void paint(Graphics g) {
		//System.out.println("RePainting");

		if(img != null) {
			Graphics g2 = img.getGraphics();
			g2.setColor(Color.black);
			g2.fillRect(0, 0, w, h);
			
			int cwt = cw;
			int cht = ch;
			int wd = 1;
			int hd = -1;
			
			tPrime = paintCount;
			drawPoint(g2, cwt, cht);
			for(int j = 1; j < 300; j++) {
				for(int xa = 1; xa <= j; xa++) {
					cwt += wd;
					drawPoint(g2, cwt, cht);
				}
				for(int ya = 1; ya <= j; ya++) {
					cht += hd;
					drawPoint(g2, cwt, cht);
				}
				wd *= -1;
				hd *= -1;
			}
		}
		g.drawImage(img, 0, 0, null);
		//System.out.println("Finished Painting");
		paintCount++;
	}
	
	private void drawPoint(Graphics g, int x, int y) {
		Prime p = primes.get(tPrime);
		if(p.isPrime) {
			g.setColor(Color.white);
		} else {
			g.setColor(Color.black);
		}
		g.drawRect(x, y+20, 1, 1);
		tPrime++;
	}
	
	private void generatePrimes() {
		System.out.println("Starting to Gen primes");
		primes = new ArrayList<Prime>();
		boolean isPrime = false;
		for(int i = 1; i < w*h*2; i++) {
			isPrime = true;
			for(int j = 2; j <= (int)Math.sqrt(i); j++) {
				if((i % j) == 0 && j != i) {
					isPrime = false;
					break;
				}
			}
			primes.add(new Prime(i, isPrime));

		}
		System.out.println("Finished Generating Primes");
	}

	static public void main(String[] args) {
		new JFrameDrawingExample();
	}

}
